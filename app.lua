local gxml = require './xml';
local json = require './json';
local timer = require 'timer';

local function XMLToTable(xml)
	local h = gxml.simpleTreeHandler()
  local x = gxml.xmlParser(h)
  x:parse(xml)
	return h.root
end

print 'Reading contents of xml.txt'
local f = io.open("xml.txt", "rb");
local content = f:read("*all");
f:close();

print 'Converting XML to a Lua table'
local raw_table = XMLToTable(content);

content = nil;

print 'Converting Lua table to JSON'
local json_output = json:encode(raw_table);

raw_table = nil;

print 'Writing JSON table to output.txt'
local f = io.open("output.txt", "w+")
f:write(json_output)
f:close();

json_output = nil;

print 'Completed'

-- To keep console window open
timer.setInterval(1000000, function() end)
